'use strict';
var mongoose = require('mongoose');
var LHcontact = mongoose.model('LHcontact');
var Lhtime = mongoose.model('Lhtime');

exports.processRequest = function(req, res) {
console.log(req.body)
if (req.body.queryResult.action == "WorkingHours")
  {
      getLhtime(req,res)
  }
    else if (req.body.queryResult.action == "StationLocation")
  {
      getLhLocation(req,res)
  }  else if (req.body.queryResult.action == "StationContact")
  {
      getLhTel(req,res)
  }
};
function getLhLocation(req,res)
{
  let parameters = req.body.queryResult.parameters;
  var addresses;
  console.log(parameters);
  let station = parameters.Station;
  console.log(station);
  console.log("okf1");
  /*LHcontact.count({Code:station}, function (err, count) {
  console.log(count);
    if (!err && count == 0) {
        return res.json({
        //speech: 'We are not open!',
        fulfillmentText: 'We do not operate from '+parameters.Station,
        source: 'LH Loc'
        });
    }
    });*/
  LHcontact.find({Code:station}, function(err,contact)
  {
      if (err)
      {
        return res.json({
        //speech: 'Something went wrong!',
        fulfillmentText: 'Something went wrong!',
        source: 'LH Loc'
        });
      }
    console.log(contact);
   var address;
    for (var i=0; i < contact.length; i++)
       {
        console.log("okf04");
        var contact1 = contact[i];
        
		var street = contact1.Street;
		var city =  contact1.City;
        var state = contact1.State;
        var zip = contact1.Zip;
		var country = contact1.Country;
		address = street+ "\n" + city +  "\n" + state +  "\n" + zip +  "\n" + country + "\n";
		
        }
     return res.json({
        //speech: 'Something went right!',
        fulfillmentText: 'Station address is ' + address,
        source: 'LH Loc'
        });   
   })
}
function getLhTel(req,res)
{
  let parameters = req.body.queryResult.parameters;
  var addresses;
  console.log(parameters);
  let station = parameters.Station;
  console.log(station);
  console.log("okc1");
  /*LHcontact.count({Code:station}, function (err, count) {
  console.log(count);
    if (!err && count == 0) {
        return res.json({
        //speech: 'We are not open!',
        fulfillmentText: 'We do not operate from '+parameters.Station,
        source: 'LH Tel'
        });
    }
    });*/
  LHcontact.find({Code:station}, function(err,contact)
  {
      if (err)
      {
        return res.json({
        //speech: 'Something went wrong!',
        fulfillmentText: 'Something went wrong!',
        source: 'LH Tel'
        });
      }
    console.log(contact);

     return res.json({
        //speech: 'Something went right!',
        fulfillmentText: 'Station phone number is ' + contact[0].Phone + ' and email id is ' + contact[0].Email,
        source: 'LH Tel'
        });   
   })
}
function getLhtime(req,res)
{
  let parameters = req.body.queryResult.parameters;
  console.log(parameters);
  let to_from = parameters.To_From;
  //let city = parameters.geo-city;
  let day = parameters.Day;
  let station = parameters.Station;
  console.log(day);
  console.log("ok1");
  Lhtime.count({Code:station}, function (err, count) {
  console.log(count);
    if (!err && count == 0) {
        return res.json({
        //speech: 'We are not open!',
        fulfillmentText: 'We do not operate from '+parameters.Station,
        source: 'LH Time'
        });
    }
    });
  //if (parameters.Station == "")
  Lhtime.find({$and : [{ Code:station },{ Day:day}]},function(err,timings)
  //Lhtime.find({Day:day},function(err,timings)
  {
      if (err)
      {
        return res.json({
        //speech: 'Something went wrong!',
        fulfillmentText: 'Something went wrong!',
        source: 'LH Time'
        });
      }
    console.log(timings);
    var timing = timings[0];
    //console.log(timings.count);
    //console.log(timings.size);
     if(timing.To == timing.From)
    {
        return res.json({
        //speech: 'We are not open!',
        fulfillmentText: 'We are not open on '+parameters.Day,
        source: 'LH Time'
        });
    }
    for (var i=0; i < timings.length; i++)
       {
        console.log("ok04");
        var timing = timings[i];
        console.log(timing.From);
        }
     return res.json({
        //speech: 'Something went right!',
        fulfillmentText: 'Station ' +timing.Code+' is open on '+timing.Day+' between '+timing.From+' and '+timing.To+'. Do you want to book a frieght?',
        source: 'LH Time'
        });   
   })
}
