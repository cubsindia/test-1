var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var LHcontact = new Schema({
Code:{
	type:String,
	required:false
},
Phone:{
	type:String,
	required:false
},
Email:{
	type:String,
	required:false
},
Service:{
	type:String,
	required:false
},
Street:{
	type:String,
	required:false
},
City:{
	type:String,
	required:false
},
State:{
type:String,
required:false
},
Zip:{
	type:String,
	required:false
},
Country:{
	type:String,
	required:false
}
});
module.exports = mongoose.model('LHcontact', LHcontact);
