var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Lhtime = new Schema({
Code:{
 type:String,
 required:false
},
Day:{
 type:String,
 required:false
},
From:{
 type:String,
 required:false
},
To:{
 type:String,
 required:false
},
Service:{
 type:String,
 required:false
}
});
module.exports = mongoose.model('Lhtime', Lhtime);